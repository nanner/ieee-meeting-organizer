package com.example.madc;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import java.util.ArrayList;
import java.util.Locale;

public class SectionsPagerAdapter extends FragmentPagerAdapter {

    private static final int OVERVIEW_INDEX = 0;
    private static final int LIST_INDEX = 1;
    @SuppressWarnings("unused")
    private static final int STATISTICS_INDEX = 2;

    private Activity activity;
    private Context context;

    public MeetingListFragment meetingListFragment;

    public SectionsPagerAdapter(FragmentManager fm, Context context, Activity activity) {
        super(fm);
        this.context = context;
        this.activity = activity;
    }

    @Override
    public Fragment getItem(int position) {
        // getItem is called to instantiate the fragment for the given page.
        // Return a MeetingOverviewFragment (defined as a static inner class
        // below) with the page number as its lone argument.

        Fragment fragment;
        Bundle args = new Bundle();
        if(position == LIST_INDEX){
            meetingListFragment = new MeetingListFragment(context, activity);
            fragment = meetingListFragment;
            args.putInt(MeetingListFragment.ARG_SECTION_NUMBER, position + 1);
        }
        else if(position == OVERVIEW_INDEX){
            fragment = new MeetingOverviewFragment(context);
            args.putInt(MeetingOverviewFragment.ARG_SECTION_NUMBER, position + 1);
        }
        else
        {
            fragment = new MeetingStatisticsFragment(context);
            args.putInt(MeetingOverviewFragment.ARG_SECTION_NUMBER, position + 1);
        }

        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public int getCount() {
        // Show 3 total pages.
        return 3;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        Locale l = Locale.getDefault();
        switch (position) {
            case 0:
                return context.getString(R.string.title_section1).toUpperCase(l);
            case 1:
                return context.getString(R.string.title_section2).toUpperCase(l);
            case 2:
                return context.getString(R.string.title_section3).toUpperCase(l);
        }
        return null;
    }


}