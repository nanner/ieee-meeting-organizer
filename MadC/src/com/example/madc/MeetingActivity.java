package com.example.madc;

import android.app.Activity;
import android.content.Context;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.GridLayout;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

public class MeetingActivity extends Activity {

    private Meeting meeting;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.meeting_layout);

        int position = getIntent().getExtras().getInt(MainActivity.MEETING_POSITION);

        meeting = MeetingSearch.getMeetings().get(position);

        Toast.makeText(this, meeting.getTitle(), Toast.LENGTH_SHORT).show();

        TextView meetingView = (TextView) findViewById(R.id.item_title);
        meetingView.setText(meeting.getTitle());

        TextView dateView = (TextView) findViewById(R.id.item_date);
        dateView.setText(meeting.getFormattedStartDate());

        TextView descriptionView = (TextView) findViewById(R.id.item_description);
        descriptionView.setText(meeting.getDescription());

        TextView locationView = (TextView) findViewById(R.id.item_location);
        locationView.setText(meeting.getLocation());

        ListView listView = (ListView) findViewById(R.id.item_attendees);
        listView.setAdapter(new AttendeeListAdapter(this, meeting.getAttendees()));


	}

    private class AttendeeListAdapter extends ArrayAdapter<Attendee> {
        private final Context context;
        private ArrayList<Attendee> attendees;

        public AttendeeListAdapter(Context context, ArrayList<Attendee> attendees) {
            super(context, R.layout.attendee_list_item, attendees);
            this.context = context;
            this.attendees = attendees;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            Attendee attendee = attendees.get(position);

            LayoutInflater layoutInflater = LayoutInflater.from(context);

            GridLayout attendeeLayout =
                    (GridLayout) layoutInflater.inflate(R.layout.attendee_list_item, null);

            ImageView presenceView = (ImageView) attendeeLayout.findViewById(R.id.icon);

            if (attendee.hasAccepted() ) {
                int imageResource = getResources().getIdentifier("android:drawable/presence_online", null, null);
                presenceView.setImageResource(imageResource);
            }

            if (attendee.hasDeclined() ) {
                int imageResource = getResources().getIdentifier("android:drawable/presence_offline", null, null);
                presenceView.setImageResource(imageResource);
            }

            TextView attendeeName = (TextView) attendeeLayout.findViewById(R.id.attendee_name);
            attendeeName.setText(attendee.getName());

            TextView attendeeEmail = (TextView) attendeeLayout.findViewById(R.id.attendee_email);
            attendeeEmail.setText(attendee.getEmail());
            return attendeeLayout;
        }
    }
}
