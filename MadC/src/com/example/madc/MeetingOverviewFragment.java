package com.example.madc;

import java.util.ArrayList;
import java.util.Date;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

@SuppressLint("ValidFragment")
public class MeetingOverviewFragment extends Fragment {

    public static final String ARG_SECTION_NUMBER = "section_number";
    private ArrayList<Meeting> meetings;
    private Context context;

    public MeetingOverviewFragment(Context context) {
        meetings = MeetingSearch.getMeetings();
        this.context = context;
    }

    public int getLastMeetingIndex(){
        Meeting tailMeeting = meetings.get(meetings.size() - 1);

        long time = new Date().getTime();

        if(tailMeeting.getEndDate() > time)
            return -1;

        int result = 0;
        for(int i = 0; i < meetings.size(); i++){
            long test = meetings.get(i).getEndDate();
            if(test < time){
                result = i;
                break;
            }
        }

        return result;
    }

    public ArrayList<Meeting> getUpcomingMeetings(){
        ArrayList<Meeting> recentMeetings = new ArrayList<Meeting>();

        Date d = new Date();
        long time = d.getTime();

        for(Meeting m : meetings){
            if(m.getEndDate() > time){
                recentMeetings.add(m);
            }
        }

        return recentMeetings;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        this.setRetainInstance(true);
        View rootView = inflater.inflate(R.layout.meeting_overview,
                container, false);

        RelativeLayout lastMeetingLayout = (RelativeLayout) rootView.findViewById(R.id.last_meeting_layout);
        TextView meetingView = (TextView)rootView.findViewById(R.id.meeting);
        TextView dateView = (TextView)rootView.findViewById(R.id.date);

        String title = getString(R.string.no_event_found);
        String start = getString(R.string.no_date);

        try {
            final int lastMeetingIndex = this.getLastMeetingIndex();
            if (lastMeetingIndex > 0) {
                Meeting lastMeeting = meetings.get(lastMeetingIndex);
                title = lastMeeting.getTitle();
                start = lastMeeting.getFormattedStartDate();
                lastMeetingLayout.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent viewMeeting = new Intent(context, MeetingActivity.class);
                        viewMeeting.putExtra(MainActivity.MEETING_POSITION, lastMeetingIndex);
                        startActivity(viewMeeting);
                    }
                });
            }
        } catch (Exception e) {
            Log.i("Exception e ",e.toString());
        }

        meetingView.setText(title);
        dateView.setText(start);

        TextView upcomingMeeting = (TextView)rootView.findViewById(R.id.upcoming_meeting);
        final TextView meetingView2 = (TextView)rootView.findViewById(R.id.meeting2);
        final TextView dateView2 = (TextView)rootView.findViewById(R.id.date2);

        final ArrayList<Meeting> upcomingMeetings = getUpcomingMeetings();

        final CycleRecentMeetings cycle = new CycleRecentMeetings(meetingView2, dateView2, upcomingMeetings);

        if (upcomingMeetings.size() > 0) {
            cycle.start();
        }

        upcomingMeeting.setText(upcomingMeeting.getText()+" ("+upcomingMeetings.size()+")");

        RelativeLayout upcomingMeetingLayout = (RelativeLayout) rootView.findViewById(R.id.upcoming_meetings_layout);

        upcomingMeetingLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Meeting meeting = cycle.getUpcomingMeeting();
                if (meeting != null) {
                    Intent viewMeeting = new Intent(context, MeetingActivity.class);
                    viewMeeting.putExtra(MainActivity.MEETING_POSITION, meetings.indexOf(meeting));
                    startActivity(viewMeeting);
                }
            }
        });

        return rootView;
    }

    private class CycleRecentMeetings extends Thread {

        Meeting upcoming;
        String title2 = getString(R.string.no_event_found);
        String start2 = getString(R.string.no_date);
        int meetingIndex;
        ArrayList<Meeting> upcomingMeetings;
        TextView meetingView2;
        TextView dateView2;

        public Meeting getUpcomingMeeting() {
            return upcoming;
        }

        CycleRecentMeetings(TextView meetingView2, TextView dateView2, ArrayList<Meeting> upcomingMeetings) {
            meetingIndex = 0;
            this.upcomingMeetings = upcomingMeetings;
            this.meetingView2 = meetingView2;
            this.dateView2 = dateView2;
        }

        @Override
        public void run() {
            while(getActivity() != null){
                try {
                    upcoming = upcomingMeetings.get(meetingIndex);

                    try{
                        title2 = upcoming.getTitle();
                        start2 = upcoming.getFormattedEndDate();
                    } catch (Exception e) {
                        Log.i("Exception e2 ",e.toString());
                    }
                    getActivity().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            meetingView2.setText(title2);
                            dateView2.setText(start2);
                        }
                    });

                    if(upcomingMeetings.size()-1 == meetingIndex)
                        meetingIndex = 0;
                    else
                        meetingIndex++;
                    Thread.sleep(2000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}