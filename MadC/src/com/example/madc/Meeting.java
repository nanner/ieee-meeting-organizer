package com.example.madc;

import java.text.Format;
import java.util.ArrayList;
import java.util.Date;

import android.app.Activity;
import android.database.Cursor;
import android.text.format.DateFormat;

public class Meeting {
    private String title;
    private String organizer;
    private String description;
    private Date startDate;
    private Date endDate;
    private String location;
    private ArrayList<Attendee> attendees = new ArrayList<Attendee>();
    private Activity activity;

    public Meeting(Cursor meetingInfo, Cursor attendeesCursor, Activity activity) {
        this.title = meetingInfo.getString(0);
        this.organizer = meetingInfo.getString(1);
        this.description = meetingInfo.getString(2);
        this.startDate = new Date(meetingInfo.getLong(3));
        this.endDate = new Date(meetingInfo.getLong(4));
        this.location = meetingInfo.getString(5);
        this.activity = activity;
        if(attendeesCursor.getCount()>0) {
            if(attendeesCursor.moveToFirst()) {
                do {
                    if (attendeesCursor.getString(1).equals("")) continue;
                    attendees.add(new Attendee(attendeesCursor));
                } while(attendeesCursor.moveToNext());
            }
        }
    }

    public String getTitle() {
        return title;
    }

    public String getOrganizer() {
        return organizer;
    }

    public String getDescription() {
        return description;
    }

    public long getStartDate() {
        return startDate.getTime();
    }

    public long getEndDate() {
        return endDate.getTime();
    }

    public String getLocation() {
        return location;
    }

    private String getFormattedDate(long date, Activity act) {
        Format df = DateFormat.getDateFormat(act);
        Format tf = DateFormat.getTimeFormat(act);

        return(tf.format(date) + " - " + df.format(date));
    }

    public String getFormattedStartDate() {
        return getFormattedDate(startDate.getTime(), activity);

    }

    public String getFormattedEndDate() {
        return getFormattedDate(endDate.getTime(), activity);
    }

    public ArrayList<Attendee> getAttendees() {
        return attendees;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (obj == null || obj.getClass() != this.getClass()) {
            return false;
        }

        Meeting meeting = (Meeting) obj;

        if (meeting.getStartDate() == this.getStartDate()
                && meeting.getEndDate() == this.getEndDate()
                && meeting.getTitle().equals(this.getTitle())) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result
                + ((this.getTitle() == null) ? 0 : this.getTitle().hashCode());
        result = prime * result + (int)this.getStartDate() + (int)this.getEndDate();
        return result;
    }



}
