package com.example.madc;


import android.app.Activity;
import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.ListFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import java.util.ArrayList;

@SuppressLint("ValidFragment")
public class MeetingListFragment extends ListFragment {
	
	public interface SelectionListener {
		public void onItemSelected(int position);
	}
	
    Context context;
    
    SelectionListener mCallback;

    public static final String ARG_SECTION_NUMBER = "section_number";

    public MeetingListAdapter meetingListAdapter;

    public MeetingListFragment(Context context, Activity activity) {
        this.context = context;
        mCallback = (SelectionListener) activity;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.setRetainInstance(true);

        View rootView = inflater.inflate(R.layout.meeting_list, container, false);

        return rootView;
    }
    
    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        meetingListAdapter = new MeetingListAdapter(context);
        ListView listView = getListView();
        listView.setAdapter(meetingListAdapter);
    };
    
    
    @Override
    public void onListItemClick(ListView l, View v, int position, long id) {
    	mCallback.onItemSelected(position);
    }

    public ArrayList<Meeting> getMeetings() { return meetingListAdapter.getMeetings(); }
}
