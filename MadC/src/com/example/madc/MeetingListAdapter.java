package com.example.madc;

import java.util.ArrayList;
import java.util.Date;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class MeetingListAdapter extends BaseAdapter {

    Context context;

	public ArrayList<Meeting> meetings;

	public MeetingListAdapter(Context context) {
        this.context = context;
        this.meetings = MeetingSearch.getMeetings();
    }
	
	@Override
	public int getCount() {
		return meetings.size();
	}

	@Override
	public Object getItem(int position) {
		return meetings.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {

        Meeting meeting = (Meeting) getItem(position);
		
		LayoutInflater layoutInflater = LayoutInflater.from(context);
		RelativeLayout meetingLayout =
                (RelativeLayout) layoutInflater.inflate(R.layout.meeting_list_item, null);

        long currentTime = new Date().getTime();

        if (currentTime > meeting.getEndDate()) {
            meetingLayout.setBackgroundResource(R.drawable.greyed_out_selector);
        }


        TextView meetingTitleView = (TextView) meetingLayout.findViewById(R.id.meeting_title);
		meetingTitleView.setText(meeting.getTitle());
		
		TextView meetingDateView = (TextView) meetingLayout.findViewById(R.id.meeting_date);
		meetingDateView.setText(meeting.getFormattedStartDate());
		
		return meetingLayout;
	}

    public ArrayList<Meeting> getMeetings() {
        return meetings;
    }
    
}