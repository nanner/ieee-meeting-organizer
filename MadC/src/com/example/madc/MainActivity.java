package com.example.madc;

import android.app.ActionBar;
import android.app.ActionBar.Tab;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.view.ViewPager;
import android.view.Menu;
import android.view.MenuItem;

public class MainActivity extends FragmentActivity implements
		ActionBar.TabListener, MeetingListFragment.SelectionListener {

    public static final String MEETING_POSITION = "POS";
    SectionsPagerAdapter mSectionsPagerAdapter;

	ViewPager mViewPager;

    private static final int RESULT_SETTINGS = 1;


	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

        MeetingSearch.initializeMeetingSearch(getApplicationContext(), this);

        // Set up the action bar.
		final ActionBar actionBar = getActionBar();
		actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);

		mSectionsPagerAdapter = new SectionsPagerAdapter(
                getSupportFragmentManager(),
                getApplicationContext(),
                this);

		mViewPager = (ViewPager) findViewById(R.id.pager);
		mViewPager.setAdapter(mSectionsPagerAdapter);

		mViewPager
				.setOnPageChangeListener(new ViewPager.SimpleOnPageChangeListener() {
					@Override
					public void onPageSelected(int position) {
						actionBar.setSelectedNavigationItem(position);
					}
				});

		// For each of the sections in the app, add a tab to the action bar.
		for (int i = 0; i < mSectionsPagerAdapter.getCount(); i++) {
			actionBar.addTab(actionBar.newTab()
					.setText(mSectionsPagerAdapter.getPageTitle(i))
					.setTabListener(this));
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

    @Override
    public boolean onMenuItemSelected(int featureId, MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_settings:
                Intent i = new Intent(this, Settings.class);
                startActivityForResult(i, RESULT_SETTINGS);
                break;
        }

        return true;
    }

    @Override
	public void onTabReselected(Tab arg0, android.app.FragmentTransaction arg1) {
		// nothingness
	}

	@Override
	public void onTabSelected(Tab tab, android.app.FragmentTransaction arg1) {
		// When the given tab is selected, switch to the corresponding page in
		// the ViewPager.
		mViewPager.setCurrentItem(tab.getPosition());
	}

	@Override
	public void onTabUnselected(Tab arg0, android.app.FragmentTransaction arg1) {
        // nothingness
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        switch (requestCode) {
            case RESULT_SETTINGS:
                MeetingSearch.updateMeetings();
                mSectionsPagerAdapter.meetingListFragment.meetingListAdapter.notifyDataSetChanged();
                break;
        }
    }

	@Override
	public void onItemSelected(int position) {
		Intent viewMeeting = new Intent(this, MeetingActivity.class);
        viewMeeting.putExtra(MEETING_POSITION, position);
		startActivity(viewMeeting);
	}
}
