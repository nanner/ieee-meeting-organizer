package com.example.madc;

import android.database.Cursor;
import android.provider.CalendarContract;

public class Attendee {
	private String name;
	private String email;
    private int status;
	
	public Attendee(Cursor attendeesCursor) {
		this.name = attendeesCursor.getString(1);
		this.email = attendeesCursor.getString(2);
        this.status = attendeesCursor.getInt(3);
	}
	
	public String getName() {
		return name;
	}
	
	public String getEmail() {
		return email;
	}

    public int getStatus() { return status; }

    public boolean hasAccepted() {
        if (this.status == CalendarContract.Attendees.ATTENDEE_STATUS_ACCEPTED)
            return true;
        else
            return false;
    }

    public boolean hasDeclined() {
        if (this.status == CalendarContract.Attendees.ATTENDEE_STATUS_DECLINED)
            return true;
        else
            return false;
    }
}
