package com.example.madc;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.database.Cursor;
import android.preference.PreferenceManager;
import android.provider.CalendarContract;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map.Entry;

public class MeetingSearch {
    private static final String PREF_TRUSTED_ORGANIZER = "trustedOrganizer";
    private static final String PREF_MEETING_IDENTIFIER = "meetingIdentifier";

    private static final String DEFAULT_MEETING_IDENTIFIER = "ieee";
    private static ArrayList<Meeting> meetings = new ArrayList<Meeting>();

    private static Cursor eventsCursor = null;
    private static Cursor attendeesCursor = null;

    private static final int TITLE_INDEX = 0;
    private static final int ORGANIZER_INDEX = 1;
    private static final int EVENT_ID_INDEX = 6;

    private static final String[] MEETING_COLS = new String[] {
            CalendarContract.Events.TITLE,
            CalendarContract.Events.ORGANIZER,
            CalendarContract.Events.DESCRIPTION,
            CalendarContract.Events.DTSTART,
            CalendarContract.Events.DTEND,
            CalendarContract.Events.EVENT_LOCATION,
            CalendarContract.Events._ID
    };

    private static final String[] ATTENDEE_COLS = new String[] {
            CalendarContract.Attendees.EVENT_ID,
            CalendarContract.Attendees.ATTENDEE_NAME,
            CalendarContract.Attendees.ATTENDEE_EMAIL,
            CalendarContract.Attendees.ATTENDEE_STATUS
    };

    private static final String eventIdQuery = "(" + CalendarContract.Attendees.EVENT_ID + " = ?)";

    private static String meetingIdentifier = DEFAULT_MEETING_IDENTIFIER;
    private static String trustedOrganizer;

    private static Context context;
    private static Activity activity;
    private static SharedPreferences prefs;

    public static void initializeMeetingSearch(Context c, Activity a) {
        context = c;
        activity = a;

        prefs = PreferenceManager
                .getDefaultSharedPreferences(context);

        createTrustedOrganizer();
        searchMeetings();
    }

    private static void searchMeetings() {

        meetings.clear();

        meetingIdentifier = prefs.getString(PREF_MEETING_IDENTIFIER, DEFAULT_MEETING_IDENTIFIER);

        eventsCursor = context.getContentResolver()
                .query(CalendarContract.Events.CONTENT_URI, MEETING_COLS, null, null, null);


        if (eventsCursor != null) {
            eventsCursor.moveToFirst();
            while(eventsCursor.moveToNext()) {
                String eventName = eventsCursor.getString(TITLE_INDEX);
                String organizer = eventsCursor.getString(ORGANIZER_INDEX);

                if (eventName == null) continue;

                if (organizer.toLowerCase().contains(trustedOrganizer.toLowerCase())
                        || eventName.toLowerCase().contains(meetingIdentifier.toLowerCase())) {
                    String[] eventId = new String[] {eventsCursor.getString(EVENT_ID_INDEX)};

                    attendeesCursor = context.getContentResolver()
                            .query(CalendarContract.Attendees.CONTENT_URI,
                                    ATTENDEE_COLS, eventIdQuery, eventId, null);

                    Meeting meeting = new Meeting(eventsCursor, attendeesCursor, activity);
                    meetings.add(meeting);
                }

            }
        }

        removeDuplicateMeetings();

        Collections.sort(meetings, new MeetingComparator());
    }

    private static void removeDuplicateMeetings() {
        boolean removeDuplicates = prefs.getBoolean("removeDuplicates", false);
        if (removeDuplicates) {
            HashSet<Meeting> hashSet = new HashSet<Meeting>();
            hashSet.addAll(meetings);
            meetings.clear();
            meetings.addAll(hashSet);
        }
    }


    private static void createTrustedOrganizer() {

        if (!trustedOrganizerIsSet()) {
            eventsCursor = context.getContentResolver()
                    .query(CalendarContract.Events.CONTENT_URI, MEETING_COLS, null, null, null);

            HashMap<String, Integer> organizers = new HashMap<String, Integer>();

            if (eventsCursor != null) {
                eventsCursor.moveToFirst();
                while(eventsCursor.moveToNext()) {
                    String eventName = eventsCursor.getString(TITLE_INDEX);
                    if (eventName.toLowerCase().contains(meetingIdentifier.toLowerCase())) {
                        String organizer = eventsCursor.getString(ORGANIZER_INDEX);

                        if ( organizers.containsKey(organizer) ) {
                            organizers.put(organizer, organizers.get(organizer) + 1);
                        } else {
                            organizers.put(organizer, 1);
                        }
                    }
                }
            }

            Entry<String,Integer> maxEntry = null;

            for(Entry<String,Integer> entry : organizers.entrySet()) {
                if (maxEntry == null || entry.getValue() > maxEntry.getValue()) {
                    maxEntry = entry;
                }
            }
            if (maxEntry != null)
                trustedOrganizer = maxEntry.getKey();

            Editor editor = prefs.edit();
            editor.putString(PREF_TRUSTED_ORGANIZER, trustedOrganizer);
            editor.commit();
        }
    }

    private static boolean trustedOrganizerIsSet() {
        trustedOrganizer = prefs.getString(PREF_TRUSTED_ORGANIZER, "");
        return !trustedOrganizer.equals("");
    }

    public static void updateMeetings() {
        searchMeetings();
    }

    public static ArrayList<Meeting> getMeetings() {
        return meetings;
    }

    public static class MeetingComparator implements Comparator<Meeting> {
        @Override
        public int compare(Meeting s, Meeting t) {
            if (s.getStartDate() < t.getStartDate())
                return 1;
            else if (s.getStartDate() > t.getStartDate())
                return -1;
            else
                return 0;
        }
    }
}
