package com.example.madc;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

public class MeetingStatisticsFragment extends Fragment {
    private ArrayList<Meeting> meetings;
    private Context context;

    public MeetingStatisticsFragment(Context context) {
        meetings = MeetingSearch.getMeetings();
        this.context = context;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        this.setRetainInstance(true);
        View rootView = inflater.inflate(R.layout.statistics,
                container, false);

        return rootView;
    }
}
